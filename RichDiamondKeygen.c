#define _POSIX
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <memory.h>
#include <time.h>
#include <math.h>
#include <omp.h>

static const uint32_t VersionKey = 0xCE9190D8; //0x72C53A70; // Is sometimes changed if debugged;
static const int MaxKeys = 1; // 1: print only Program-Key, 23: print Program-Key and Level-Keys
static const bool PrintStatus = false; // print status every 0x1000000 iterations
static const bool ShowBrokenRdLevSetKeys = false; // list matches with Keys that only work in RdLevSet

static const uint64_t start = 0;
//static const uint64_t end = 0x8000000000;
static const uint64_t end = 0x1000000000000ULL; // 48-bit


// ' ' '-' -> block seperators-->ignored
// base 24 without vowels
static const char Base24Array[24] = {
  '0', '1', '2', '3', '4', '5', '6', '7',
  '8', '9', 'B', 'C', 'D', 'F', 'G', 'H',
  'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R'
};
static const uint8_t ConversionTable[5][24] = {
 {0xFF, 0x0C, 0xFF, 0xFF, 0x02, 0x09, 0x0D, 0xFF,
  0x01, 0x05, 0x07, 0x04, 0x0B, 0x03, 0x06, 0x0F,
  0xFF, 0x00, 0x0E, 0xFF, 0xFF, 0xFF, 0x0A, 0x08},

 {0x04, 0xFF, 0x03, 0x0A, 0x02, 0x07, 0xFF, 0x00,
  0xFF, 0xFF, 0x0C, 0x0D, 0x0E, 0x01, 0x0F, 0x08,
  0x09, 0x05, 0x0B, 0xFF, 0xFF, 0x06, 0xFF, 0xFF},

 {0x01, 0x08, 0xFF, 0x0C, 0x07, 0xFF, 0x02, 0x03,
  0x0B, 0x06, 0x0F, 0x09, 0x0A, 0x0E, 0xFF, 0xFF,
  0xFF, 0xFF, 0x04, 0xFF, 0x00, 0x0D, 0x05, 0xFF},

 {0xFF, 0x0D, 0x06, 0x09, 0x02, 0xFF, 0x05, 0x0C,
  0x0A, 0x01, 0xFF, 0xFF, 0x0B, 0xFF, 0xFF, 0x04,
  0x0F, 0xFF, 0x07, 0x08, 0x0E, 0xFF, 0x03, 0x00},

 {0x0C, 0x04, 0xFF, 0x09, 0x07, 0x06, 0xFF, 0xFF,
  0xFF, 0x0D, 0x0E, 0xFF, 0x02, 0xFF, 0xFF, 0x03,
  0x05, 0x0A, 0x00, 0x08, 0x0F, 0x0B, 0xFF, 0x01}
};
static const uint8_t ArraySelectors[5][24] = {
 {0xFF, 0x01, 0xFF, 0xFF, 0x02, 0x04, 0x01, 0xFF,
  0x02, 0x00, 0x04, 0x00, 0x02, 0x00, 0x02, 0x00,
  0xFF, 0x00, 0x02, 0xFF, 0xFF, 0xFF, 0x04, 0x04},

 {0x02, 0xFF, 0x00, 0x02, 0x03, 0x02, 0xFF, 0x03,
  0xFF, 0xFF, 0x03, 0x04, 0x02, 0x02, 0x02, 0x01,
  0x03, 0x04, 0x04, 0xFF, 0xFF, 0x03, 0xFF, 0xFF},

 {0x02, 0x04, 0xFF, 0x03, 0x01, 0xFF, 0x01, 0x00,
  0x02, 0x03, 0x04, 0x03, 0x01, 0x03, 0xFF, 0xFF,
  0xFF, 0xFF, 0x01, 0xFF, 0x01, 0x04, 0x01, 0xFF},

 {0xFF, 0x01, 0x01, 0x04, 0x00, 0xFF, 0x03, 0x00,
  0x04, 0x00, 0xFF, 0xFF, 0x02, 0xFF, 0xFF, 0x04, 
  0x00, 0xFF, 0x00, 0x02, 0x03, 0xFF, 0x04, 0x02},

 {0x00, 0x02, 0xFF, 0x03, 0x04, 0x03, 0xFF, 0xFF,
  0xFF, 0x00, 0x00, 0xFF, 0x02, 0xFF, 0xFF, 0x03,
  0x00, 0x01, 0x04, 0x03, 0x01, 0x04, 0xFF, 0x03}
};
static const char * ConvertToAuthCode(uint64_t Value) {
  static char AuthCode[17];
  int i, j;
  unsigned ArraySelector = 0;
  while(Value && !(Value>>60) ) {
    Value<<=4;
  }
  for(i=0; Value; i++) {
    for(j=0; j<24; j++) {
      if(ConversionTable[ArraySelector][j] == (Value>>60)) {
        AuthCode[i] = Base24Array[j];
        ArraySelector = ArraySelectors[ArraySelector][j];
        break;
      }
    }
    Value<<=4;
  }
  AuthCode[i] = 0;
  return AuthCode;
}

static const size_t SomeByteArray1[2][8] = {
  {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07},
  {0x00, 0x02, 0x08, 0x07, 0x08, 0x05, 0x00, 0x01}
};
static const size_t SomeByteArray2[2][8] = {
  {0x05, 0x06, 0x04, 0x08, 0x07, 0x00, 0x03, 0x01},
  {0x05, 0x07, 0x00, 0x02, 0x01, 0x03, 0x03, 0x06}
};
static const size_t SomeByteArray3[2][8] = {
  {0x03, 0x08, 0x01, 0x02, 0x06, 0x04, 0x07, 0x00},
  {0x03, 0x05, 0x01, 0x03, 0x02, 0x07, 0x04, 0x00}
};
static const size_t SomeByteArray4[2][8] = {
  {0x08, 0x02, 0x06, 0x07, 0x00, 0x01, 0x05, 0x03},
  {0x08, 0x04, 0x03, 0x06, 0x04, 0x01, 0x05, 0x07}
};
static const size_t SomeByteArray5[8] = {
  0x00, 0x01, 0x02, 0x03, 0x04, 0x01, 0x03, 0x00
};

static const size_t SomeByteArray6[8] = {
  0x00, 0x01, 0x01, 0x02, 0x03, 0x04, 0x05, 0x05
};
static const size_t SomeByteArray7[8] = {
  0x04, 0x03, 0x00, 0x05, 0x01, 0x02, 0x00, 0x04
};

static const uint32_t KeyValues[23] = {
  // key for main program
  0x436F7265,

  // keys for single levels
  0x76951662, //106
  0x4721E265, //108
  0x579BF236, //109
  0x98599C6D, //110
  0x6BD8F0B9, //111
  0xBF6569E8, //112
  0xE1331E2A, //113
  0x4D02B694, //114
  0xB807175C, //115
  0xDC61F92B, //116
  0xFB64CC1D, //117
  0x5DB192C8, //118
  0x484DDFB4, //119
  0x455702CA, //121
  0x73527E14, //122
  0xDDB10EDE, //126
  0x64DE7EE8, //124
  0x28B93B85, //106

  // multi-level keys, missing: 117, 124, 126
  0xEBDAD7F1, // (106, 108, 109, 110, 111)
  0x21033974, // (106, 108, 109, 110, 111)
  0x5391E536, // (112, 113, 114, 115, 116)
  0xC7C23136  // (118, 119, 121, 122)

  //0xFB64CC1D, 0x64DE7EE8, 0xDDB10EDE, 0xEBDAD7F1, 0x5391E536, 0xC7C23136
};

static inline bool HasOddNumberOfBits(uint32_t x) {
#if defined (__GNUC__)
  return __builtin_parity(x);
#else 
  x = x ^ (x >> 16);
  x = x ^ (x >> 8);
  x = x ^ (x >> 4);
  x = x ^ (x >> 2);
  return ((x >> 1) & 1) ^ (x & 1);
#endif
}
static inline uint32_t NumberOfBits(uint32_t x) {
#if defined (__GNUC__)
  return __builtin_popcount(x);
#else 
  x = x - ((x >> 1) & 0x55555555);
  x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
  x = (x + (x >> 4)) & 0x0F0F0F0F;
  x = x + (x >> 8);
  x = x + (x >> 16);
  return x & 0x0000003F;
#endif
}

/*
isValid: 1, Time: 18323.544884 seconds, iAuthCode: 129646A25C, iAuthCode2: 12964549, Value1: 9646A25C Value2: 12 Progress: 7.260553%
isValid: 0, Time: 44753.829474 seconds, iAuthCode: 2AFFFDFBCF, iAuthCode2: 2AFFFFFF, Value1: FFFDFBCF Value2: 2A Progress: 16.796863%
0x52
*/

int main(void) {
  uint64_t iValue2, secondsTotal;
  uint64_t hours;
  unsigned int minutes, seconds;

  clock_t t1 = clock();

  iValue2 = 0;
  #pragma omp parallel for private(secondsTotal, hours, minutes, seconds) firstprivate(iValue2)
  for(uint64_t iValue=start; iValue<end; iValue++) {
    bool isValid;
    uint32_t array1[9], array2[5], array3[6];
    uint32_t Value8 = 0, Value9_12 = 0, Value10 = 0, Value11 = 0;
    uint32_t Value_8_11__1 = 0, Value_8_11__2 = 0, Value_9_12__1 = 0, Value_9_12__2 = 0, Value_10__1 = 0, Value_10__2 = 0;
    uint32_t ValueVersionKey, bv1, bv2;
    uint32_t dv1, dv2, dv3, dv4, mask;
    uint64_t Value = iValue;

    //if(!!(i == 0x3F && (HasOddNumberOfBits(Value1 & 0xFFFF0000)))) isValid = 0;
    if (
      !!(Value&0x8)^
      !!(Value&0x400)^!!(Value&0x800)^
      !!(Value&0x200000000)^!!(Value&0x400000000)^!!(Value&0x800000000)^
      !!(Value&0x8000000000)^
      !!(Value&0x20000000000)^!!(Value&0x80000000000)^
      !!(Value&0x200000000000)^!!(Value&0x800000000000)
    )
      continue;

    //if(!!(i == 0x3F && (HasOddNumberOfBits(Value1) ^ HasOddNumberOfBits(Value2)))) isValid = 0;
    if(
      /*
      !!(Value&0x4)^!!(Value&0x8)^
      !!(Value&0x10)^!!(Value&0x20)^!!(Value&0x40)^!!(Value&0x80)^
      !!(Value&0x4000)^!!(Value&0x8000)^
      !!(Value&0x100000)^!!(Value&0x200000)^
      !!(Value&0x4000000)^!!(Value&0x8000000)^
      */
      /*
      !!(Value&0x8)^
      !!(Value&0x400)^!!(Value&0x800)^
      !!(Value&0x200000000)^!!(Value&0x400000000)^!!(Value&0x800000000)^
      !!(Value&0x8000000000)^
      !!(Value&0x20000000000)^!!(Value&0x80000000000)^
      !!(Value&0x200000000000)^!!(Value&0x800000000000)^
      */
      !!(Value&0x20000)^!!(Value&0x40000)^!!(Value&0x80000)^
      !!(Value&0x800000)^
      !!(Value&0x2000000)^!!(Value&0x8000000)^
      !!(Value&0x20000000)^!!(Value&0x80000000)^
      !!(Value&0x400000000)^
      !!(Value&0x2000000000)
    )
      continue;

    //if(!!(i == 0x3F && HasOddNumberOfBits(Value2))) isValid = 0;
    if(
      !!(Value&0x4)^!!(Value&0x8)^
      !!(Value&0x10)^!!(Value&0x20)^!!(Value&0x40)^!!(Value&0x80)^
      !!(Value&0x4000)^!!(Value&0x8000)^
      !!(Value&0x100000)^!!(Value&0x200000)^
      !!(Value&0x4000000)^!!(Value&0x8000000)
    )
      continue;

    //if(!!(i == 0x3F && (Value1 & 0x2000))) isValid = 0;
    if(
      !!(Value&0x1)^!!(Value&0x8)^
      !!(Value&0x40)^!!(Value&0x80)^
      !!(Value&0x100)^!!(Value&0x200)^!!(Value&0x400)^!!(Value&0x800)^
      !!(Value&0x1000)^!!(Value&0x2000)^!!(Value&0x4000)^!!(Value&0x8000)^
      !!(Value&0x10000)^!!(Value&0x20000)^!!(Value&0x40000)^!!(Value&0x80000)^
      !!(Value&0x100000)^!!(Value&0x200000)^!!(Value&0x400000)^!!(Value&0x800000)^
      !!(Value&0x1000000)^!!(Value&0x2000000)^!!(Value&0x4000000)^!!(Value&0x8000000)^
      !!(Value&0x10000000)^!!(Value&0x20000000)^!!(Value&0x80000000)^
      !!(Value&0x100000000)^!!(Value&0x400000000)^!!(Value&0x800000000)^
      !!(Value&0x800000000000)
    )
      continue;

    //if(!!(i == 0x3F && (HasOddNumberOfBits(Value1 & 0xAAAAAAAA) ^ HasOddNumberOfBits(Value2 & 0xAAAA)))) isValid = 0;
    if(
      !!(Value&0x1)^
      !!(Value&0x10)^!!(Value&0x80)^
      !!(Value&0x800)^
      !!(Value&0x8000)^
      !!(Value&0x40000)^
      !!(Value&0x100000)^!!(Value&0x400000)^!!(Value&0x800000)^
      !!(Value&0x4000000)^
      !!(Value&0x40000000)^!!(Value&0x80000000)^
      !!(Value&0x400000000)^!!(Value&0x800000000)^
      !!(Value&0x4000000000)^!!(Value&0x8000000000)^
      !!(Value&0x40000000000)^!!(Value&0x80000000000)^
      !!(Value&0x400000000000)
    )
      continue;

    //if(!!(i == 0x3F && (HasOddNumberOfBits(Value1 & 0xCCCCCCCC) ^ HasOddNumberOfBits(Value2 & 0xCCCC)))) isValid = 0;
    if(
      !!(Value&0x1)^!!(Value&0x2)^!!(Value&0x4)^
      !!(Value&0x10)^!!(Value&0x40)^!!(Value&0x80)^
      !!(Value&0x1000)^!!(Value&0x2000)^!!(Value&0x4000)^!!(Value&0x8000)^
      !!(Value&0x80000)^
      !!(Value&0x100000)^!!(Value&0x800000)^
      !!(Value&0x8000000)^
      !!(Value&0x10000000)^!!(Value&0x20000000)^!!(Value&0x40000000)^
      !!(Value&0x100000000)^!!(Value&0x200000000)^!!(Value&0x400000000)^
      !!(Value&0x1000000000)^!!(Value&0x2000000000)^!!(Value&0x4000000000)^
      !!(Value&0x10000000000)^!!(Value&0x20000000000)^!!(Value&0x40000000000)^
      !!(Value&0x100000000000)^!!(Value&0x200000000000)^!!(Value&0x400000000000)^!!(Value&0x800000000000)
    )
      continue;

    //if(!!(i == 0x3F && (HasOddNumberOfBits(Value1 & 0xF0F0F0F0) ^ HasOddNumberOfBits(Value2 & 0xF0F0)))) isValid = 0;
    if(
      !!(Value&0x1)^!!(Value&0x4)^
      !!(Value&0x20)^
      !!(Value&0x100)^!!(Value&0x200)^!!(Value&0x400)^!!(Value&0x800)^
      !!(Value&0x1000)^!!(Value&0x2000)^
      !!(Value&0x10000)^!!(Value&0x20000)^!!(Value&0x40000)^!!(Value&0x80000)^
      !!(Value&0x100000)^
      !!(Value&0x2000000)^!!(Value&0x8000000)^
      !!(Value&0x40000000)^!!(Value&0x80000000)^
      !!(Value&0x200000000)^!!(Value&0x400000000)^
      !!(Value&0x4000000000)^!!(Value&0x8000000000)^
      !!(Value&0x20000000000)^!!(Value&0x40000000000)^
      !!(Value&0x400000000000)
    )
      continue;

    //if(!!(i == 0x3F && (HasOddNumberOfBits(Value1 & 0xFF00FF00) ^ HasOddNumberOfBits(Value2 & 0xFF00)))) isValid = 0;
    if(
      !!(Value&0x8)^
      !!(Value&0x1000)^!!(Value&0x2000)^!!(Value&0x4000)^!!(Value&0x8000)^
      !!(Value&0x40000)^!!(Value&0x80000)^
      !!(Value&0x100000)^!!(Value&0x200000)^
      !!(Value&0x2000000)^
      !!(Value&0x80000000)^
      !!(Value&0x400000000)^
      !!(Value&0x2000000000)^
      !!(Value&0x20000000000)^!!(Value&0x40000000000)^!!(Value&0x80000000000)^
      !!(Value&0x800000000000)
    )
      continue;

    memset(array1, 0, sizeof(array1));
    for(int i=0; i<0x60; i++) {
      uint64_t tmp = !!(Value&0x01)^!!(Value&0x02)^!!(Value&0x08)^!!(Value&0x10)^!!(Value&0x40)^!!(Value&0x800000000000);
      Value = (Value >> 1) | (tmp << 47);

      uint32_t Value1 = Value & 0xFFFFFFFF;
      uint32_t Value2 = (Value >> 32) & 0xFFFF;
      switch(i) {
        case 0x0F:
          //Value_10__1 = (Value1 >> 12) & 3;
          //array3[0x05] = ((Value1 >> 19) & 1) | ((Value1 >> 2) & 2);
          //Value_9_12__1 = (Value1 >> 7) & 3;
          //Value_8_11__1 = (Value1 >> 15) & 3;
          //Value10 = ((Value2 & 0xFFFE) << 16) | (((Value1 & 0xC000) | ((Value1 >> 1) & 0x7FFF0000)) >> 14);
          break;
        case 0x1F:
          //Value_10__2 = Value1 & 3;
          //array3[0x02] = ((Value1 >> 17) & 1) | ((Value1 >> 15) & 2);
          //Value_8_11__2 = (Value1 >> 10) & 3;
          //array3[0x03] = (Value1 >> 21) & 3;
          //Value9_12 = ((Value2 & 0xFFFE) << 16) | (((Value1 & 0xC000) | ((Value1 >> 1) & 0x7FFF0000)) >> 14);
          break;
        case 0x2F:
          //Value_9_12__2 = (Value1 >> 12) & 3;
          //array3[0x01] = ((Value1 >> 1) & 1) | ((Value1 >> 9) & 2);
          //array3[0x04] = Value1 & 3;
          //array3[0x00] = (Value1 >> 17) & 3;
          Value8 = ((Value2 & 0xFFFE) << 16) | (((Value1 & 0xC000) | ((Value1 >> 1) & 0x7FFF0000)) >> 14);
          break;
        case 0x3F:
          array3[0x02] = (Value1 >> 6) & 3;
          array3[0x05] = ((Value1 >> 4) & 2) | ((Value1 >> 3) & 1);
          Value_9_12__1 = (Value1 >> 11) & 3;
          Value_9_12__2 = (Value1 >> 9) & 3;
          Value9_12 = ((Value2 & 0xFFFE) << 16) | (((Value1 & 0xC000) | ((Value1 >> 1) & 0x7FFF0000)) >> 14);
          break;
        case 0x4F:
          array3[0x00] = (Value1 >> 5) & 3;
          array3[0x04] = ((Value1 >> 19) & 2) | ((Value1 >> 11) & 1);
          Value_8_11__2 = (Value1 >> 2) & 3;
          Value_10__1 = (Value1 >> 12) & 3;
          Value10 = ((Value2 & 0xFFFE) << 16) | (((Value1 & 0xC000) | ((Value1 >> 1) & 0x7FFF0000)) >> 14);
          break;
        case 0x5F:
          Value_10__2 = (Value1 >> 18) & 3;
          array3[0x03] = ((Value1 >> 8) & 2) | ((Value1 >> 6) & 1);
          array3[0x01] = (Value1 >> 22) & 3;
          Value_8_11__1 = (Value1 >> 3) & 3;
          Value11 = ((Value2 & 0xFFFE) << 16) | (((Value1 & 0xC000) | ((Value1 >> 1) & 0x7FFF0000)) >> 14);
          break;
      }
    }

    bv1 = isValid = 0;
    dv1 = dv2 = dv3 = dv4 = 0;
    iValue2++;
    array2[0x00] = 0x80080202;
    array2[0x01] = 0x84010011;
    array2[0x02] = 0x80200401;
    array2[0x03] = 0x81000802;
    array2[0x04] = 0x90001004;

    for(int i=0, ArraySelector=0; i<13; i++, ArraySelector = !!(bv2&0x08) != !!(bv2&0x10)) {
      if(i<8) {
        ValueVersionKey = VersionKey;
        //dv1 = array1[SomeByteArray1[ArraySelector][i]];
        //dv2 = array1[SomeByteArray2[ArraySelector][i]];
        //dv3 = array1[SomeByteArray3[ArraySelector][i]];
        //dv4 = array1[SomeByteArray4[ArraySelector][i]];
        mask = array2[SomeByteArray5[i]];
        bv1 = array3[SomeByteArray6[i]];
        bv2 = array3[SomeByteArray7[i]];
      } else {
        switch(i) {
          case 8:
            ValueVersionKey = Value8 ^ array1[0x00];
            mask = 0x80200401;
            bv1 = Value_8_11__1;
            bv2 = Value_8_11__2;
            break;
          case 9:
            ValueVersionKey = Value9_12 ^ array1[0x08];
            mask = 0x80040003;
            bv1 = Value_9_12__1;
            bv2 = Value_9_12__2;
            break;
          case 10:
            ValueVersionKey = Value10 ^ array1[0x07];
            mask = 0x80040003;
            bv1 = Value_10__1;
            bv2 = Value_10__2;
            break;
          case 11:
            ValueVersionKey = Value11 ^ array1[0x04];
            mask = 0x80200401;
            bv1 = Value_8_11__1;
            bv2 = Value_8_11__2;
            break;
          case 12:
            ValueVersionKey = Value9_12 ^ array1[0x02];
            mask = 0x80040003;
            bv1 = Value_9_12__1;
            bv2 = Value_9_12__2;
            break;
          default: break;
        }
      }

      bv2 &= 3;
      for(int j=0; j<0x40; j++) {
        if(j==0x20) {
          if(bv1!=(bv2&7)) {
            bv2^=0x18;
          }
        } else if(j==0x30) {
          if(bv1<(bv2&3)) {
            bv2 ^= 0x08;
          } else if (bv1>(bv2&7)) {
            bv2 ^= 0x10;
          }
        }
        uint8_t cl = bv1 + NumberOfBits(ValueVersionKey&mask);
        ValueVersionKey = (ValueVersionKey >> 1) | (cl << 31);
        bv1 = (cl >> 1);
        if(/*j==0x07||j==0x1f||*/j==0x3a) {
          dv1 = ValueVersionKey;
        }
        if(/*j==0x17||j==0x23||*/j==0x2f) {
          dv2 = ValueVersionKey;
        }
        if(/*j==0x10||j==0x1A||*/j==0x35) {
          dv3 = ValueVersionKey;
        }
        if(/*j==0x16||j==0x29||*/j==0x2d) {
          dv4 = ValueVersionKey;
        }
        if(j==0x33||j==0x3e) {
          mask = ValueVersionKey;
        }
        if(j==0x37||j==0x38) {
          if(bv1==(bv2&3)) {
            bv2^=0x58;
          }
        }
      }

      if(i<8) {
        array1[SomeByteArray1[ArraySelector][i]] = dv1;
        array1[SomeByteArray2[ArraySelector][i]] = dv2;
        array1[SomeByteArray3[ArraySelector][i]] = dv3;
        array1[SomeByteArray4[ArraySelector][i]] = dv4;
        array2[SomeByteArray5[i]] = mask;
        array3[SomeByteArray7[i]] = bv2;
      } else {
        switch(i) {
          case 8:
            //printf("%zX: %X (3)\n", iValue, dv3);
            for(int l=0; l<MaxKeys && ShowBrokenRdLevSetKeys; l++) {
              if(dv3 == KeyValues[l]) {
                printf("Found Key @0: %u %X\n", l, KeyValues[l]);
                isValid = 1; // TODO: only works in RDLevSet.exe
              }
            }
            break;
          case 10:
            //printf("%zX: %X (4)\n", iValue, dv4);
            for(int l=0; l<MaxKeys && ShowBrokenRdLevSetKeys; l++) {
              if(dv4 == KeyValues[l]) {
                printf("Found Key @2: %u %X\n", l, KeyValues[l]);
                isValid = 1; // TODO: only works in RDLevSet.exe
              }
            }
            break;
          case 11:
            //printf("%zX: %X (1)\n", iValue, dv1);
            for(int l=0; l<MaxKeys; l++) {
              if(dv1 == KeyValues[l]) {
                printf("Found Key @3: %u %X\n", l, KeyValues[l]);
                isValid = 1;
              }
            }
            break;
          case 9:
          case 12:
            //printf("%zX: %X (2)\n", iValue, dv2);
            for(int l=0; l<MaxKeys; l++) {
              if(dv2 == KeyValues[l]) {
                printf("Found Key @1/4: %u %X\n", l, KeyValues[l]);
                isValid = 1;
              }
            }
            break;
        }
      }
    }

    if(isValid || (PrintStatus && ((iValue2&0xFFFFFF) == 0xFFFFFF))) {
      secondsTotal = (clock() - t1)/CLOCKS_PER_SEC;
      hours = (secondsTotal/60)/60;
      minutes = (secondsTotal/60)%60;
      seconds = secondsTotal%60;
      printf("isValid: %u, AuthCode: %s, iValue2: %zX, Value1: %zX, Value2: %zX, %zu:%02u:%02u hours, Progress: %lf\n", isValid, ConvertToAuthCode(iValue), iValue2, iValue&0xFFFFFFFF, iValue>>32, hours, minutes, seconds, 100*(double)(iValue - start)/(end - start));
      //printf("0: %X %X %X %X %X\n", dv3, dv2, dv4, dv1, dv2);
    }
  }

  secondsTotal = (clock() - t1)/CLOCKS_PER_SEC;
  hours = (secondsTotal/60)/60;
  minutes = (secondsTotal/60)%60;
  seconds = secondsTotal%60;
  printf("Finished in %zu:%02u:%02u hours\n", hours, minutes, seconds);

  return 0;
}
